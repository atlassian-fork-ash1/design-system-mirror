import FeedbackCollector from './components/FeedbackCollector';
import FeedbackForm from './components/FeedbackForm';
import FeedbackFlag from './components/FeedbackFlag';
export { FormFields } from './types';

export default FeedbackCollector;
export { FeedbackFlag, FeedbackForm };
