import React from 'react';

class ItemsRendererWithCustomComponents extends TypedItemsRenderer<CustomComponents> {}

const Funky = props => null;
const Groovy = props => null;

<ItemsRendererWithCustomComponents items={[]} />;
<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[]}
/>;
<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[
    { type: 'BackItem', id: 'back' },
    { type: 'ContainerHeader', id: 'header' },
    { type: 'Debug', id: 'debug', foo: 'bar' },
    {
      type: 'GoToItem',
      id: 'goto',
      goTo: 'my-view',
    },
    { type: 'GroupHeading', id: 'heading', text: 'heading' },
    { type: 'Item', id: 'item', text: 'abc' },
    { type: 'SortableItem', id: 'sortable-item', text: 'abc', index: 0 },
    { type: 'SectionHeading', id: 'heading', text: 'heading' },
    { type: 'Separator', id: 'separator' },
    { type: 'Switcher', id: 'item', target: <div />, options: [] },
    { type: 'Wordmark', id: 'item', wordmark: () => null },
    {
      type: 'Group',
      id: 'group',
      customComponents: { Funky, Groovy },
      items: [
        { type: 'Item', id: 'item', text: 'Item' },
        // Custom components should work within other items
        {
          type: 'Funky',
          id: 'funky-component',
          funky: true,
        },
      ],
    },
    {
      type: 'HeaderSection',
      id: 'headerSection',
      customComponents: { Funky, Groovy },
      items: [
        { type: 'Item', id: 'item', text: 'Item' },
        // Custom components should work within other items
        {
          type: 'Funky',
          id: 'funky-component',
          funky: true,
        },
      ],
    },
    {
      type: 'MenuSection',
      id: 'menuSection',
      customComponents: { Funky, Groovy },
      nestedGroupKey: 'menu',
      items: [
        { type: 'Item', id: 'item', text: 'Item' },
        // Custom components should work within other items
        {
          type: 'Funky',
          id: 'funky-component',
          funky: true,
        },
      ],
    },
    {
      type: 'Section',
      id: 'section',
      customComponents: { Funky, Groovy },
      nestedGroupKey: 'section',
      items: [
        { type: 'Item', id: 'item', text: 'Item' },
        // Custom components should work within other items
        {
          type: 'Funky',
          id: 'funky-component',
          funky: true,
        },
      ],
    },
    {
      type: 'SortableContext',
      id: 'sortable-context',
      customComponents: { Funky, Groovy },
      onDragEnd: () => {},
      items: [
        { type: 'Item', id: 'item', text: 'Item' },
        // Custom components should work within other items
        {
          type: 'Funky',
          id: 'funky-component',
          funky: true,
        },
      ],
    },
    {
      type: 'SortableGroup',
      id: 'sortable-group',
      customComponents: { Funky, Groovy },
      items: [
        { type: 'Item', id: 'item', text: 'Item' },
        // Custom components should work within other items
        {
          type: 'Funky',
          id: 'funky-component',
          funky: true,
        },
      ],
    },
    {
      type: 'InlineComponent',
      id: 'inline-component',
      component: () => <Funky funky />,
    },
    {
      type: 'Funky',
      id: 'funky-component',
      funky: true,
    },
    {
      type: 'Groovy',
      id: 'groovy-component',
      groovy: true,
    },
  ]}
/>;

<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[
    {
      type: 'InlineComponent',
      id: 'inline-component',
      component: () => <Funky funky />,
    },
  ]}
/>;

<ItemsRendererWithCustomComponents customComponents={{ Funky, Groovy }} />;
<ItemsRendererWithCustomComponents
  customComponents={{ Foo: null }}
  items={[]}
/>;
<CustomItemsRenderer
  customComponents={{ Funky, Groovy }}
  items={[
    {
      type: 'abc',
      id: 'abc',
    },
  ]}
/>;
<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[
    {
      type: 'Item',
    },
  ]}
/>;
<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[
    {
      type: 'GoToItem',
      id: 'goto',
    },
  ]}
/>;

<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[
    {
      type: 'GroupHeading',
      id: 'heading',
    },
  ]}
/>;

<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[
    {
      type: 'SortableItem',
      id: 'sortable',
      text: 'sortable',
    },
  ]}
/>;

<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[
    {
      type: 'Group',
      id: 'group',
    },
  ]}
/>;

<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[
    {
      type: 'Funky',
      id: 'funky-component',
    },
  ]}
/>;

<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[
    {
      type: 'Groovy',
      id: 'groovy-component',
    },
  ]}
/>;

<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[
    {
      type: 'Group',
      id: 'group',
      items: [
        {
          type: 'Groovy',
          id: 'groovy-component',
        },
      ],
    },
  ]}
/>;

<ItemsRendererWithCustomComponents
  customComponents={{ Funky, Groovy }}
  items={[
    {
      type: 'InlineComponent',
      id: 'inline-component',
    },
  ]}
/>;
