import React from 'react';
import LayoutManager from '../../index';

<LayoutManager
  containerNavigation={() => null}
  globalNavigation={() => null}
  productNavigation={() => null}
>
  Page
</LayoutManager>;

<LayoutManager
  containerNavigation={() => null}
  globalNavigation={() => null}
  productNavigation={() => null}
/>;
<LayoutManager containerNavigation={() => null} globalNavigation={() => null}>
  Page
</LayoutManager>;
<LayoutManager productNavigation={() => null} globalNavigation={() => null}>
  Page
</LayoutManager>;
<LayoutManager productNavigation={() => null} containerNavigation={() => null}>
  Page
</LayoutManager>;

<LayoutManager
  productNavigation="foo"
  globalNavigation={() => null}
  containerNavigation={() => null}
>
  Page
</LayoutManager>;
