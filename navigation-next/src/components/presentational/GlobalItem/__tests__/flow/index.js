import React from 'react';
import GlobalItem from '../../index';

<GlobalItem />;
<GlobalItem
  id="my-global-item"
  href="http://www.atlassian.com"
  label="My global item"
  onClick={() => {}}
  icon={() => null}
/>;

<GlobalItem id={5} />;
<GlobalItem icon={5} />;
