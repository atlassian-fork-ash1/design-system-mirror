import React from 'react';
import GlobalNavigationItemPrimitive from '../../primitives';

<GlobalNavigationItemPrimitive />;
<GlobalNavigationItemPrimitive
  id="my-global-item"
  href="http://www.atlassian.com"
  label="My global item"
  onClick={() => {}}
  icon={() => null}
/>;

<GlobalNavigationItemPrimitive id={5} />;
<GlobalNavigationItemPrimitive icon={5} />;
