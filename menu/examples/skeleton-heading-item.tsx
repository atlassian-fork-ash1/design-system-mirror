import React from 'react';
import { SkeletonHeadingItem } from '../src';

export default () => <SkeletonHeadingItem isShimmering />;
