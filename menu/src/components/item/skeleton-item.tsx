/** @jsx jsx */
import { jsx } from '@emotion/core';

import { itemSkeletonCSS } from './styles';
import { SkeletonItemProps } from '../types';

const SkeletonItem = ({
  hasAvatar,
  hasIcon,
  width,
  testId,
  isShimmering,
}: SkeletonItemProps) => (
  <div
    css={itemSkeletonCSS(hasAvatar, hasIcon, width, isShimmering)}
    data-testid={testId}
  />
);

export default SkeletonItem;
