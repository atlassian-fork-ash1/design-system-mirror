/** @jsx jsx */
import { jsx, CSSObject } from '@emotion/core';

import { linkItemCSS } from './styles';
import { LinkItemProps } from '../types';
import BaseItem from './base-item';

const LinkItem = ({ href, ...rest }: LinkItemProps) => {
  const {
    children,
    cssFn = (currentStyles: CSSObject) => currentStyles,
    description,
    elemAfter,
    elemBefore,
    isDisabled = false,
    isSelected = false,
    onClick,
    testId,
    ...others
  } = rest;

  if (!children) {
    return null;
  }

  const Container = isDisabled ? 'span' : 'a';

  return (
    <Container
      css={cssFn(linkItemCSS(isDisabled, isSelected), {
        isSelected,
        isDisabled,
      })}
      href={isDisabled ? undefined : href}
      data-testid={testId}
      onClick={isDisabled ? undefined : onClick}
      {...others}
    >
      <BaseItem
        elemBefore={elemBefore}
        elemAfter={elemAfter}
        description={description}
      >
        {children}
      </BaseItem>
    </Container>
  );
};

export default LinkItem;
