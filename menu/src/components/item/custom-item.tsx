import React from 'react';
import { CSSObject } from '@emotion/core';
import { ClassNames } from '@emotion/core';

import { customItemCSS } from './styles';
import { CustomItemProps } from '../types';
import BaseItem from './base-item';

const CustomItem = ({
  component: Component,
  cssFn = (currentStyles: CSSObject) => currentStyles,
  isDisabled = false,
  isSelected = false,
  onClick,
  testId,
  ...rest
}: CustomItemProps) => {
  if (!Component) {
    return null;
  }

  return (
    <ClassNames>
      {({ css }) => (
        <Component
          wrapperClass={css(
            cssFn(customItemCSS(isDisabled, isSelected), {
              isDisabled,
              isSelected,
            }),
          )}
          data-testid={testId}
          onClick={isDisabled ? undefined : onClick}
        >
          <BaseItem {...rest} />
        </Component>
      )}
    </ClassNames>
  );
};

export default CustomItem;
