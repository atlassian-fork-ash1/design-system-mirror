/** @jsx jsx */
import { jsx, CSSObject } from '@emotion/core';
import { forwardRef } from 'react';
import { buttonItemCSS } from './styles';

import { ButtonItemProps } from '../types';
import BaseItem from './base-item';

const ButtonItem = forwardRef<HTMLElement, ButtonItemProps>((props, ref) => {
  const {
    children,
    cssFn = (currentStyles: CSSObject) => currentStyles,
    description,
    elemAfter,
    elemBefore,
    isDisabled = false,
    isSelected = false,
    onClick,
    testId,
    ...others
  } = props;

  if (!children) {
    return null;
  }

  const Container = isDisabled ? 'span' : 'button';

  return (
    <Container
      type={isDisabled ? undefined : 'button'}
      css={cssFn(buttonItemCSS(isDisabled, isSelected), {
        isSelected,
        isDisabled,
      })}
      data-testid={testId}
      onClick={isDisabled ? undefined : onClick}
      // We want the ref to be a HTMLElement instead of a specific HTMLDivElement so we cast to any to work around this.
      ref={ref as any}
      {...others}
    >
      <BaseItem
        elemBefore={elemBefore}
        elemAfter={elemAfter}
        description={description}
      >
        {children}
      </BaseItem>
    </Container>
  );
});

export default ButtonItem;
