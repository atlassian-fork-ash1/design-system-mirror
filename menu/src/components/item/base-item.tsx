/** @jsx jsx */
import { jsx } from '@emotion/core';

import {
  elemBeforeCSS,
  elemAfterCSS,
  descriptionCSS,
  contentCSS,
  truncateCSS,
  contentCSSWrapper,
} from './styles';
import { BaseItemProps } from '../types';

const BaseItem = ({
  children,
  description,
  elemAfter,
  elemBefore,
}: BaseItemProps) => {
  return (
    <div css={contentCSSWrapper}>
      {elemBefore && (
        <span data-item-elem-before css={elemBeforeCSS}>
          {elemBefore}
        </span>
      )}
      {children && (
        <span css={contentCSS}>
          <span css={truncateCSS}>{children}</span>
          {description && (
            <span data-item-description css={descriptionCSS}>
              {description}
            </span>
          )}
        </span>
      )}
      {elemAfter && (
        <span data-item-elem-after css={elemAfterCSS}>
          {elemAfter}
        </span>
      )}
    </div>
  );
};

export default BaseItem;
