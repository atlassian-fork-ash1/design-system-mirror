import React from 'react';
import Textfield from '../src';

export default function() {
  return (
    <div>
      <label htmlFor="basic">Basic text field</label>
      <Textfield name="basic" />
    </div>
  );
}
