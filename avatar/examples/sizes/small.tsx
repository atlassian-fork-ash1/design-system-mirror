import React from 'react';
import Avatar from '../../src';

const Small = () => (
  <div>
    <Avatar name="small" size="small" />
  </div>
);

export default Small;
